#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxKinect.h"
#include "ofxGui.h"

class testApp : public ofBaseApp
{
public:
	void setup();
	void update();
	void draw();
	void exit();
	
	void keyPressed(int key);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	
private:
    void kinectAngleChanged(int& angle);
    
	ofxKinect kinect;
	
    ofSoundPlayer sound;
	
	ofxCvColorImage colorImg;
	ofxCvGrayscaleImage grayImage; // grayscale depth image
	ofxCvGrayscaleImage grayThreshNear; // the near thresholded image
	ofxCvGrayscaleImage grayThreshFar; // the far thresholded image
	ofxCvContourFinder contourFinder;
    
    vector<float> amplitudes;
    vector<ofxCvBlob> blobs;
	
    ofParameter<float> nearThreshold;
    ofParameter<float> farThreshold;
    ofParameter<int> minContourArea;
	
	ofParameter<int> angle;
    bool drawGui;
    
    ofxPanel gui;
};
