#include "testApp.h"


//--------------------------------------------------------------
void testApp::setup()
{
    ofSetFrameRate(30);
    
	ofSetLogLevel(OF_LOG_VERBOSE);
    
    sound.loadSound("beat.wav");
    sound.setLoop(true);
    sound.setVolume(0);
    sound.play();
	
	// enable depth->video image calibration
	kinect.setRegistration(true);
    
	kinect.init();
	
	kinect.open();		// opens first available kinect
	
	// print the intrinsic IR sensor values
	if(kinect.isConnected())
    {
		ofLogNotice() << "sensor-emitter dist: " << kinect.getSensorEmitterDistance() << "cm";
		ofLogNotice() << "sensor-camera dist:  " << kinect.getSensorCameraDistance() << "cm";
		ofLogNotice() << "zero plane pixel size: " << kinect.getZeroPlanePixelSize() << "mm";
		ofLogNotice() << "zero plane dist: " << kinect.getZeroPlaneDistance() << "mm";
	}
	
	colorImg.allocate(kinect.width, kinect.height);
	grayImage.allocate(kinect.width, kinect.height);
	grayThreshNear.allocate(kinect.width, kinect.height);
	grayThreshFar.allocate(kinect.width, kinect.height);
	
	nearThreshold = 230;
	farThreshold = 70;
	
	ofSetFrameRate(60);
	
	// zero the tilt on startup
	angle = 0;
	kinect.setCameraTiltAngle(angle);
    
    drawGui = true;
    gui.setup();
    gui.add(nearThreshold.set("near threshold", 255, 0, 255));
    gui.add(farThreshold.set("far threshold", 0, 0, 255));
    gui.add(minContourArea.set("min contour area", 10000, 0, 100000));
    gui.add(angle.set("kinect angle", 0, -30, 30));
    gui.loadFromFile("settings.xml");
    
    kinect.setCameraTiltAngle(angle);
    
    angle.addListener(this, &testApp::kinectAngleChanged);
    
}

//--------------------------------------------------------------
void testApp::update()
{	
	ofBackground(100, 100, 100);
	
	kinect.update();
	
	// there is a new frame and we are connected
	if(kinect.isFrameNew())
    {
		
		// load grayscale depth image from the kinect source
		grayImage.setFromPixels(kinect.getDepthPixels(), kinect.width, kinect.height);
		
		// we do two thresholds - one for the far plane and one for the near plane
		// we then do a cvAnd to get the pixels which are a union of the two thresholds
        grayThreshNear = grayImage;
        grayThreshFar = grayImage;
        grayThreshNear.threshold(nearThreshold, true);
        grayThreshFar.threshold(farThreshold);
        cvAnd(grayThreshNear.getCvImage(), grayThreshFar.getCvImage(), grayImage.getCvImage(), NULL);
        
		
		// update the cv images
		grayImage.flagImageChanged();
		
		// find contours which are between the size of 20 pixels and 1/3 the w*h pixels.
		// also, find holes is set to true so we will get interior contours as well....
		contourFinder.findContours(grayImage, minContourArea, (kinect.width*kinect.height)/2, 4, false);
        
        amplitudes.resize(contourFinder.nBlobs);
        if (blobs.empty())
        {
            blobs = contourFinder.blobs;
        }
        else
        {
            for (unsigned i = 0; i < contourFinder.nBlobs; ++i)
            {
                float distance = 0;
                if (i < blobs.size())
                {
                    distance = 100.f * fabs(blobs[i].centroid.x - contourFinder.blobs[i].centroid.x);
                    amplitudes[i] = 0.99 * distance + 0.01 * amplitudes[i];
                    //if (0.2 * amplitudes[i] > 100.f) amplitudes[i] = 0.f;
                }
            }
            if (!amplitudes.empty())
            {
                float amplitude = 0;
                for (unsigned i = 0; i < amplitudes.size(); ++i)
                {
                    amplitude += ofMap(amplitudes[i], 0, 200, 0, 1, true);
                }
                sound.setVolume(amplitude / (float)amplitudes.size());
            }
            else sound.setVolume(0);
            blobs = contourFinder.blobs;
        }
	}
}

//--------------------------------------------------------------
void testApp::draw() {
	
	ofSetColor(255, 255, 255);
    
    ofBackgroundGradient(ofColor(255, 255, 0), ofColor(255, 127, 127));
    ofSetLineWidth(2);
    ofPushStyle();
    for (unsigned i = 0; i < contourFinder.nBlobs; ++i)
    {
        ofPoint centroid = contourFinder.blobs[i].centroid;
        ofPushMatrix();
        ofTranslate(ofGetWidth() * centroid.x / 640.0, 0);
        ofSetColor(255, 0, 255);
        ofLine(-100, 0, -100, ofGetHeight());
        ofLine(100, 0, 100, ofGetHeight());
        ofSetColor(0, 0, 255);
        ofNoFill();
        ofBeginShape();
        for (int j = 0; j < ofGetHeight(); j += 2)
        {
            ofVertex(0.2f * amplitudes[i] * sin(0.1 * j + 10.f * ofGetElapsedTimef()), j);
        }
        ofEndShape();
        ofPopMatrix();
    }
    ofPopStyle();
    
    ofSetLineWidth(1);
    
    if (drawGui)
    {
        grayImage.draw(ofGetWidth() - 330, 10, 320, 240);
        contourFinder.draw(ofGetWidth() - 330, 10, 320, 240);
        gui.draw();
    }

	// draw instructions
	ofSetColor(0);
	ofDrawBitmapString("SPACE to toggle GUI, f to toggle full screen", 10, ofGetHeight() - 10);
}

void testApp::kinectAngleChanged(int& angle)
{
    kinect.setCameraTiltAngle(angle);
}

//--------------------------------------------------------------
void testApp::exit()
{
	kinect.setCameraTiltAngle(0); // zero the tilt on exit
	kinect.close();
}

//--------------------------------------------------------------
void testApp::keyPressed (int key)
{
	switch (key)
    {
        case 'f':
            ofToggleFullscreen();
            break;
            
        case 'g':
        case ' ':
            drawGui = !drawGui;
            break;
            
		case '>':
		case '.':
			farThreshold ++;
			if (farThreshold > 255) farThreshold = 255;
			break;
			
		case '<':
		case ',':
			farThreshold --;
			if (farThreshold < 0) farThreshold = 0;
			break;
			
		case '+':
		case '=':
			nearThreshold ++;
			if (nearThreshold > 255) nearThreshold = 255;
			break;
			
		case '-':
			nearThreshold --;
			if (nearThreshold < 0) nearThreshold = 0;
			break;
			
		case 'w':
			kinect.enableDepthNearValueWhite(!kinect.isDepthNearValueWhite());
			break;
			
		case 'o':
			kinect.setCameraTiltAngle(angle); // go back to prev tilt
			kinect.open();
			break;
			
		case 'c':
			kinect.setCameraTiltAngle(0); // zero the tilt
			kinect.close();
			break;
			
		case '1':
			kinect.setLed(ofxKinect::LED_GREEN);
			break;
			
		case '2':
			kinect.setLed(ofxKinect::LED_YELLOW);
			break;
			
		case '3':
			kinect.setLed(ofxKinect::LED_RED);
			break;
			
		case '4':
			kinect.setLed(ofxKinect::LED_BLINK_GREEN);
			break;
			
		case '5':
			kinect.setLed(ofxKinect::LED_BLINK_YELLOW_RED);
			break;
			
		case '0':
			kinect.setLed(ofxKinect::LED_OFF);
			break;
			
		case OF_KEY_UP:
			angle++;
			if(angle>30) angle=30;
			kinect.setCameraTiltAngle(angle);
			break;
			
		case OF_KEY_DOWN:
			angle--;
			if(angle<-30) angle=-30;
			kinect.setCameraTiltAngle(angle);
			break;
	}
}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button)
{}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button)
{}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button)
{}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{}
